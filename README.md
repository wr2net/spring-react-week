# Semana Spring React
#### Realização: [DevSuperior - Escola de Programação](https://devsuperior.com.br/)

## Episódio 1 - Spring e React no mercado
Descubra o poder dessa stack de desenvolvimento de sistemas. Spring e React pagam bem e são as ferramentas mais adotadas do mercado para back end e front end respectivamente.

## Episódio 2 - Aprofundando na prática
Agora que nosso app já está com o layout estático estruturado, vamos mergulhar no back end. E você vai ver o quanto é fácil fazer isso no Spring.

## Episódio 3 - O mapa da carreira
Neste episódio vamos te mostrar um mapa de estudos para carreira, e vamos terminar de construir nosso app. Vamos também apresentar nossos cursos completos.

### Backend
Desenvolvido em Java com Spring Boot Framework.

### Frontend
Desenvolvido em TypeScript/Javascript com ReactJs Framework.

### Postman
Disponibilizado as coleções e os environments para apontamento do servidor backend no localhost e em produção para realização dos testes e visualização das requisições.

### Backend em produção: <https://wr2net-dsmovie.herokuapp.com/> 
### Frontend em produção: <https://wr2net-dsmovie.netlify.app/>
